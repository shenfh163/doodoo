const schedule = require("node-schedule");
const moment = require("moment");
const Doodoo = require("doodoo");

const app = new Doodoo();
app.core();

doodoo.ding("系统提示", "计划任务启动成功");

// 发货十天后自动确认收货
async function check_order_send() {
    const data = moment()
        .subtract(10, "days")
        .format("YYYY-MM-DD HH:mm:ss");
    const nowdata = moment().format("YYYY-MM-DD HH:mm:ss");
    await model("order")
        .query(qb => {
            qb.where("status", 1);
            qb.where("send_time", "<", data);
            qb.update({ status: 2, finsh_time: nowdata });
        })
        .fetchAll();
}
async function check_order_back() {
    const data = moment()
        .subtract(7, "days")
        .format("YYYY-MM-DD HH:mm:ss");
    const nowdata = moment().format("YYYY-MM-DD HH:mm:ss");
    await model("order")
        .query(qb => {
            qb.where("status", 2);
            qb.where("finsh_status", 0);
            qb.where("finsh_time", "<", data);
            qb.update({ finsh_status: 1 });
        })
        .fetchAll();
}

// 每天6:00自动更新小程序
schedule.scheduleJob("0 0 0 * * *", async () => {
    await check_order_send();
    await check_order_back();
});
