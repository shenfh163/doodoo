const base = require("./../base");
const moment = require("moment");

module.exports = class extends base {
    async _initialize() {
        await super.isWxaAuth();
        await super.isShopAuth();
    }

    /**
     *
     * @api {get} /shop/api/shop/fullCut/index 可用满减
     * @apiDescription 可用满减
     * @apiGroup Shop/api
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Referer 小程序referer.
     *
     * @apiParam {Number} totalprice   订单总价
     *
     * @apiSampleRequest /shop/api/shop/fullCut/index
     *
     */
    async index() {
        const shopId = this.state.shop.id;

        const fullCut = await this.model("fullcut")
            .query(qb => {
                qb.where("shop_id", shopId);
                qb.where("status", 1);
                qb.orderBy("limit_price", "desc");
            })
            .fetchAll();
        this.success(fullCut);
    }
};
