const base = require("./../../base");
const moment = require("moment");

module.exports = class extends base {
    async _initialize() {
        await super.isCustomAuth();
        await super.isAppAuth();
        await super.isShopAuth();
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/kuaidi/index 快递列表
     * @apiDescription 快递列表
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} page  页码
     * @apiParam {String} name  快递名称
     *
     * @apiSampleRequest /shop/home/shop/plugin/kuaidi/index
     *
     */

    async index() {
        const shopId = this.state.shop.id;
        const { page = 1, name } = this.query;
        const kuaidi = await this.model("kuaidi")
            .query(qb => {
                if (this.isSet(name)) {
                    qb.where("delivery_name", "like", `%${name}%`);
                }
                qb.orderBy("id", "desc");
            })
            .fetchPage({
                pageSize: 20,
                page: page
            });
        this.success(kuaidi);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/kuaidi/shop 店铺快递列表(分页)
     * @apiDescription 店铺快递列表(分页)
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} page  页码
     *
     * @apiSampleRequest /shop/home/shop/plugin/kuaidi/shop
     *
     */

    async shop() {
        const shopId = this.state.shop.id;
        const { page = 1 } = this.query;
        const kuaidi = await this.model("shop_kuaidi")
            .query(qb => {
                qb.where("shop_id", shopId);
                qb.orderBy("id", "desc");
            })
            .fetchPage({
                pageSize: 20,
                page: page
            });
        this.success(kuaidi);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/kuaidi/shopAll 店铺快递列表(不分页)
     * @apiDescription 店铺快递列表(不分页)
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     *
     * @apiSampleRequest /shop/home/shop/plugin/kuaidi/shopAll
     *
     */

    async shopAll() {
        const shopId = this.state.shop.id;
        const kuaidi = await this.model("shop_kuaidi")
            .query(qb => {
                qb.where("shop_id", shopId);
                qb.orderBy("id", "desc");
            })
            .fetchAll();
        this.success(kuaidi);
    }
    /**
     *
     * @api {post} /shop/home/shop/plugin/kuaidi/add 新增/修改店铺快递
     * @apiDescription 新增/修改店铺快递
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     *
     * @apiSampleRequest /shop/home/shop/plugin/kuaidi/add
     *
     */

    async add() {
        const shopId = this.state.shop.id;
        const data = this.post;
        if (data.del.length) {
            await this.model("shop_kuaidi")
                .query(qb => {
                    qb.whereIn("delivery_id", data.del);
                })
                .destroy({ hardDelete: true });
        }
        let kuaidi = "";
        if (data.add.length) {
            const arr = [];
            for (const x in data.add) {
                const obj = {
                    shop_id: shopId,
                    delivery_id: data.add[x].id,
                    delivery_code: data.add[x].delivery_code,
                    delivery_name: data.add[x].delivery_name
                };
                arr.push(obj);
            }
            if (arr.length) {
                kuaidi = await doodoo.bookshelf.Collection.extend({
                    model: this.model("shop_kuaidi")
                })
                    .forge(arr)
                    .invokeThen("save");
            }
        }
        this.success(kuaidi);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/kuaidi/update 修改店铺快递状态
     * @apiDescription 修改店铺快递状态
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id      店铺快递id
     * @apiParam {Number} status  活动状态（1:开启，0：关闭）
     *
     * @apiSampleRequest /shop/home/shop/plugin/kuaidi/update
     *
     */
    async update() {
        const { id, status } = this.query;
        const kuaidi = await this.model("shop_kuaidi")
            .forge({
                id,
                status
            })
            .save();
        this.success(kuaidi);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/kuaidi/del 删除店铺快递
     * @apiDescription 删除店铺快递
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id      店铺快递id
     *
     * @apiSampleRequest /shop/home/shop/plugin/kuaidi/del
     *
     */
    async del() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const kuaidi = await this.model("shop_kuaidi")
            .query(qb => {
                qb.where("id", id);
            })
            .destroy();
        this.success(kuaidi);
    }
};
