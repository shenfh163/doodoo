const base = require("./../../base");
const moment = require("moment");

module.exports = class extends base {
    async _initialize() {
        await super.isCustomAuth();
        await super.isAppAuth();
        await super.isShopAuth();
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/coupon/index 优惠券列表
     * @apiDescription 优惠券列表
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} page  页码
     *
     * @apiSampleRequest /shop/home/shop/plugin/coupon/index
     *
     */

    async index() {
        const shopId = this.state.shop.id;
        const { page = 1 } = this.query;
        await this.model("coupon")
            .query(qb => {
                qb.where("shop_id", shopId);
                qb.where(
                    "ended_at",
                    "<",
                    moment().format("YYYY-MM-DD HH:mm:ss")
                );
                qb.update({ status: 0 });
            })
            .fetchAll();
        const coupon = await this.model("coupon")
            .query(qb => {
                qb.where("shop_id", shopId);
                qb.orderBy("id", "desc");
            })
            .fetchPage({
                pageSize: 20,
                page: page
            });
        this.success(coupon);
    }

    /**
     *
     * @api {post} /shop/home/shop/plugin/coupon/add 新增/修改优惠券
     * @apiDescription 新增/修改优惠券
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id          优惠券id
     * @apiParam {String} name        优惠券名称
     * @apiParam {Number} price       优惠券价格
     * @apiParam {String} img_url     优惠券图片
     * @apiParam {Number} limit_price 优惠券使用价格
     * @apiParam {Number} limit_num   优惠券限制使用的数量（0不限制）
     * @apiParam {Number} num         优惠券数量（0不限制）
     * @apiParam {Number} has_num     优惠券已领取数量
     * @apiParam {String} started_at  活动开始时间
     * @apiParam {String} ended_at    活动结束时间
     * @apiParam {Number} status      活动状态（1:开启，0：关闭）
     *
     * @apiSampleRequest /shop/home/shop/plugin/coupon/add
     *
     */

    async add() {
        const shopId = this.state.shop.id;
        const {
            id,
            name,
            price,
            img_url,
            limit_price,
            limit_num = 0,
            num = 0,
            has_num = 0,
            started_at,
            ended_at,
            status = 1
        } = this.post;

        const coupon = await this.model("coupon")
            .forge(
                Object.assign(
                    {
                        id,
                        name,
                        price,
                        img_url,
                        limit_price,
                        limit_num,
                        num,
                        has_num,
                        started_at,
                        ended_at,
                        status
                    },
                    {
                        shop_id: shopId
                    }
                )
            )
            .save();
        this.success(coupon);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/coupon/info 优惠券详情
     * @apiDescription 优惠券详情
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id  优惠券id
     *
     * @apiSampleRequest /shop/home/shop/plugin/coupon/info
     *
     */
    async info() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const coupon = await this.model("coupon")
            .query(qb => {
                qb.where("id", id);
                qb.where("shop_id", shopId);
            })
            .fetch();
        this.success(coupon);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/coupon/update 修改优惠券状态
     * @apiDescription 修改优惠券状态
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id      优惠券id
     * @apiParam {Number} status  活动状态（1:开启，0：关闭）
     *
     * @apiSampleRequest /shop/home/shop/plugin/coupon/update
     *
     */
    async update() {
        const { id, status } = this.query;
        const coupon = await this.model("coupon")
            .forge({
                id,
                status
            })
            .save();
        this.success(coupon);
    }

    /**
     *
     * @api {get} /shop/home/shop/plugin/coupon/del 删除优惠券
     * @apiDescription 删除优惠券
     * @apiGroup Shop/Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Token 用户授权token.
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} id      优惠券id
     *
     * @apiSampleRequest /shop/home/shop/plugin/coupon/del
     *
     */
    async del() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const coupon = await this.model("coupon")
            .query(qb => {
                qb.where("id", id);
            })
            .destroy();
        this.success(coupon);
    }
};
