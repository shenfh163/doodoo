const ump = require("./ump");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "ump_trade",
    hasTimestamps: true,
    ump: function() {
        return this.belongsTo(ump, "ump_id");
    }
});
