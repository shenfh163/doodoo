const ump = require("./ump");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "app_ump",
    hasTimestamps: true,
    ump: function() {
        return this.belongsTo(ump, "ump_id");
    }
});
